
<!-- saved from url=(0096)https://gist.github.com/jybox/5362919/raw/4fadd8e2fe667a47f5041de9fba56b19542c9db1/taskinfo2.php -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">#yddContainer{display:block;font-family:Microsoft YaHei;position:relative;width:100%;height:100%;top:-4px;left:-4px;font-size:12px;border:1px solid}#yddTop{display:block;height:22px}#yddTopBorderlr{display:block;position:static;height:17px;padding:2px 28px;line-height:17px;font-size:12px;color:#5079bb;font-weight:bold;border-style:none solid;border-width:1px}#yddTopBorderlr .ydd-sp{position:absolute;top:2px;height:0;overflow:hidden}.ydd-icon{left:5px;width:17px;padding:0px 0px 0px 0px;padding-top:17px;background-position:-16px -44px}.ydd-close{right:5px;width:16px;padding-top:16px;background-position:left -44px}#yddKeyTitle{float:left;text-decoration:none}#yddMiddle{display:block;margin-bottom:10px}.ydd-tabs{display:block;margin:5px 0;padding:0 5px;height:18px;border-bottom:1px solid}.ydd-tab{display:block;float:left;height:18px;margin:0 5px -1px 0;padding:0 4px;line-height:18px;border:1px solid;border-bottom:none}.ydd-trans-container{display:block;line-height:160%}.ydd-trans-container a{text-decoration:none;}#yddBottom{position:absolute;bottom:0;left:0;width:100%;height:22px;line-height:22px;overflow:hidden;background-position:left -22px}.ydd-padding010{padding:0 10px}#yddWrapper{color:#252525;z-index:10001;background:url(chrome-extension://eopjamdnofihpioajgfdikhhbobonhbb/ab20.png);}#yddContainer{background:#fff;border-color:#4b7598}#yddTopBorderlr{border-color:#f0f8fc}#yddWrapper .ydd-sp{background-image:url(chrome-extension://eopjamdnofihpioajgfdikhhbobonhbb/ydd-sprite.png)}#yddWrapper a,#yddWrapper a:hover,#yddWrapper a:visited{color:#50799b}#yddWrapper .ydd-tabs{color:#959595}.ydd-tabs,.ydd-tab{background:#fff;border-color:#d5e7f3}#yddBottom{color:#363636}#yddWrapper{min-width:250px;max-width:400px;}</style></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">&lt;?php
/*
精英王子, m@jybox.net
http://jyprince.me/
2013.4.11
GPLv3

只支持Linux环境, 需开启短标记, 需启用`shell_exec()`.
仅支持各浏览器的最新版本, IE最低支持IE9.
后面落款处有作者的统计代码, 可删去.
*/

global $shellResult;

$shellResult["free"] = shell_exec("free -m");
$shellResult["uptime"] = shell_exec("uptime");
$shellResult["ps"] = shell_exec("ps xufwa");

?&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
    &lt;meta charset="utf-8"&gt;
    &lt;title&gt;状态监视器&lt;/title&gt;
    &lt;link href="http://lib.sinaapp.com/js/bootstrap/latest/css/bootstrap.min.css" rel="stylesheet" type="text/css"/&gt;
    &lt;link href="http://lib.sinaapp.com/js/bootstrap/latest/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/&gt;
    &lt;style type="text/css"&gt;
        /* 左侧导航栏 */
        .sidenav {
            width: 175px;
            margin: 30px 0 0;
            padding: 0;
            background-color: white;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, .065);
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, .065);
            box-shadow: 0 1px 4px rgba(0, 0, 0, .065);
        }
        .sidenav &gt; li &gt; a {
            display: block;
            margin: 0 0 -1px;
            padding: 8px 14px;
            border: 1px solid #E5E5E5;
        }
        .sidenav i {
            float: right;
            margin-top: 2px;
            margin-right: -6px;
            opacity: .25;
        }
        .sidenav &gt; li:first-child &gt; a {
            -webkit-border-radius: 6px 6px 0 0;
            -moz-border-radius: 6px 6px 0 0;
            border-radius: 6px 6px 0 0;
        }
        /* 全局常规 */
        body {
            font-family: "Microsoft YaHei", "WenQuanYi Micro Hei", "WenQuanYi Zen Hei", arial, sans-serif;
            font-size: 16px;
        }
        section {
            padding-top: 30px;
            line-height: 30px;
        }
        section header {
            font-size: 36px;
            line-height: 40px;
            font-weight: bold;
            padding-bottom: 9px;
            margin: 20px 0 30px;
            border-bottom: 1px solid #EEE;
        }
        pre, .code {
            font-family: 'Courier New', Courier, monospace;
            color: #008200;
        }
        /* 重载bootstrap*/
        .accordion-group {
            border: none;
        }
        .progress, .bar.pull-left {
            font-size: 16px;
            line-height: 20px;
            text-align: center;
        }
        .procTable td {
            white-space: nowrap;
            font-size: 12px;
            padding: 1px;
        }
    &lt;/style&gt;
&lt;/head&gt;
&lt;body data-spy="scroll" data-target=".sidenav-bar" screen_capture_injected="true" class="well"&gt;
&lt;div class="container"&gt;
    &lt;div class="row-fluid"&gt;
        &lt;div class="span2 sidenav-bar"&gt;
            &lt;ul data-spy="affix" class="nav nav-list sidenav"&gt;
                &lt;li class="active"&gt;&lt;a href="#memory"&gt;&lt;i class="icon-chevron-right"&gt;&lt;/i&gt; 内存&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#system"&gt;&lt;i class="icon-chevron-right"&gt;&lt;/i&gt; 系统&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#users"&gt;&lt;i class="icon-chevron-right"&gt;&lt;/i&gt; 用户&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href="#process"&gt;&lt;i class="icon-chevron-right"&gt;&lt;/i&gt; 进程&lt;/a&gt;&lt;/li&gt;
            &lt;/ul&gt;
        &lt;/div&gt;
        &lt;div class="span10"&gt;
            &lt;section id="memory"&gt;
                &lt;?php
                preg_match("/Mem:\\s+(?P&lt;total&gt;\\d+)\\s+(?P&lt;used&gt;\\d+)\\s+(?P&lt;free&gt;\\d+)\\s+(?P&lt;shared&gt;\\d+)\\s+(?P&lt;buffers&gt;\\d+)\\s+(?P&lt;cached&gt;\\d+)/", $shellResult["free"], $memInfo);
                preg_match("/Swap:\\s+(?P&lt;swapTotal&gt;\\d+)\\s+(?P&lt;swapUsed&gt;\\d+)\\s+(?P&lt;swapFree&gt;\\d+)/", $shellResult["free"], $r);
                $memInfo = array_merge($memInfo, $r);

                $realMemUsed = $memInfo["used"] - $memInfo["shared"] - $memInfo["buffers"] - $memInfo["cached"];
                $realMemPer = round($realMemUsed / $memInfo["total"] * 100);
                $cachedMemPer = round($memInfo["cached"] / $memInfo["total"] * 100);
                $buffersMemPer = round($memInfo["buffers"] / $memInfo["total"] * 100);
                $swapUsedMemPer = round($memInfo["swapUsed"] / $memInfo["swapTotal"] * 100);
                ?&gt;
                &lt;header&gt;内存&lt;/header&gt;
                &lt;div class="progress progress-striped active"&gt;
                    &lt;div class="bar pull-left" style="width:&lt;?= $realMemPer; ?&gt;%;background-color:#dd514c;"&gt;
                        &lt;?= $realMemPer; ?&gt;%
                    &lt;/div&gt;
                    &lt;div class="bar pull-left" style="width:&lt;?= $cachedMemPer; ?&gt;%;background-color:#5BC0DE;"&gt;
                        &lt;?= $cachedMemPer; ?&gt;%
                    &lt;/div&gt;
                    &lt;div class="bar pull-left" style="width:&lt;?= $buffersMemPer; ?&gt;%;background-color:#62C462;"&gt;
                        &lt;?= $buffersMemPer; ?&gt;%
                    &lt;/div&gt;
                    &lt;?= (100 - $realMemPer - $cachedMemPer - $buffersMemPer);?&gt;%
                &lt;/div&gt;
                &lt;? if($swapUsedMemPer &gt; 1): ?&gt;
                    &lt;div class="progress progress-striped active"&gt;
                        &lt;div class="bar pull-left"
                             style="width:&lt;?php echo $swapUsedMemPer; ?&gt;%;background-color:#FBB450;"&gt;
                            &lt;?= $swapUsedMemPer; ?&gt;%
                        &lt;/div&gt;
                        &lt;?= (100 - $swapUsedMemPer);?&gt;%
                    &lt;/div&gt;
                &lt;? endif;?&gt;
                &lt;span class="label label-important"&gt;应用程序使用内存&lt;/span&gt;
                &lt;span class="label label-info"&gt;页面缓存&lt;/span&gt;
                &lt;span class="label label-success"&gt;磁盘缓冲&lt;/span&gt;
                &lt;? if($swapUsedMemPer &gt; 1): ?&gt;
                    &lt;span class="label label-warning"&gt;交换空间&lt;/span&gt;
                &lt;? endif; ?&gt;
                &lt;span class="label"&gt;空闲内存&lt;/span&gt;
                &lt;hr/&gt;
                &lt;div class="accordion-group"&gt;
                    &lt;div class="accordion-heading"&gt;
                        &lt;a class="btn btn-info" data-toggle="collapse" href="#preFree"&gt;
                            查看原始命令
                        &lt;/a&gt;
                    &lt;/div&gt;
                    &lt;div id="preFree" class="accordion-body collapse"&gt;
                        &lt;pre&gt;&lt;?php echo htmlspecialchars($shellResult["free"]);?&gt;&lt;/pre&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/section&gt;
            &lt;section id="system"&gt;
                &lt;?php
                $uptimeInfo = explode(" ", file_get_contents("/proc/uptime"));
                $mins = trim($uptimeInfo[0]) / 60;
                $hours = $mins / 60;
                $days = floor($hours / 24);
                $hours = floor($hours - ($days * 24));
                $min = floor($mins - ($days * 60 * 24) - ($hours * 60));
                $uptimeStr = "";
                if($days)
                    $uptimeStr = "{$days} days ";
                if($hours)
                    $uptimeStr .= "{$hours} hours ";
                $uptimeStr .= "{$min} mins";

                $loadavgInfo = explode(" ", file_get_contents("/proc/loadavg"));
                $loadavgInfo = array_chunk($loadavgInfo, 4);
                $loadavgInfo = implode(" ", $loadavgInfo[0]);

                $loadavgInfo = str_replace(" ",'&lt;/span&gt; &lt;span class="label label-info"&gt;', $loadavgInfo);
                $loadavgStr = "&lt;span class='label label-info'&gt; {$loadavgInfo} &lt;/span&gt;";
                ?&gt;
                &lt;header&gt;系统&lt;/header&gt;
                &lt;table class="table table-striped table-bordered table-condensed"&gt;
                    &lt;tbody&gt;
                    &lt;tr&gt;&lt;td&gt;服务器时间&lt;/td&gt;&lt;td&gt;&lt;?= date("Y-n-j H:i:s");?&gt;&lt;/td&gt;&lt;/tr&gt;
                    &lt;tr&gt;&lt;td&gt;已运行时间&lt;/td&gt;&lt;td&gt;&lt;?= $uptimeStr;?&gt;&lt;/td&gt;&lt;/tr&gt;
                    &lt;tr&gt;&lt;td&gt;系统负荷&lt;/td&gt;&lt;td&gt;&lt;?= $loadavgStr;?&gt;&lt;/td&gt;&lt;/tr&gt;
                    &lt;/tbody&gt;
                &lt;/table&gt;
                &lt;hr /&gt;
                &lt;div class="accordion-group"&gt;
                    &lt;div class="accordion-heading"&gt;
                        &lt;a class="btn btn-info" data-toggle="collapse" href="#preUptime"&gt;
                            查看原始命令
                        &lt;/a&gt;
                    &lt;/div&gt;
                    &lt;div id="preUptime" class="accordion-body collapse"&gt;
                        &lt;pre&gt;&lt;?php echo htmlspecialchars($shellResult["uptime"]);?&gt;&lt;/pre&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/section&gt;
            &lt;section id="users"&gt;
                &lt;header&gt;用户&lt;/header&gt;
                &lt;?php
                $userInfo = array();
                $procOutput = "";
                $psInfo = $shellResult["ps"];
                $psArray = explode("\n", $psInfo);
                for($i=1; $i&lt;count($psArray); $i++)
                {
                    $vpsArrayInfo = $psArray[$i];
                    while(stripos($psArray[$i], "  "))
                        $psArray[$i] = str_replace("  ", " ", $psArray[$i]);
                    $procArray = explode(" ", $psArray[$i]);

                    @$userInfo[$procArray[0]]["cpuPer"] += $procArray[2];
                    @$userInfo[$procArray[0]]["memPer"] += $procArray[3];
                    @$userInfo[$procArray[0]]["swapMem"] += $procArray[4];
                    @$userInfo[$procArray[0]]["realMem"] += $procArray[5];
                    @$userInfo[$procArray[0]]["procNum"]++;

                    @$escedprocArray7 = htmlspecialchars($procArray[7]);
                    $cmd = str_replace(" ", "&amp;nbsp;", htmlspecialchars(substr($vpsArrayInfo,64)));

                    if(@$procArray[5] &gt; 0)
                    {
                        @$procOutput .= &lt;&lt;&lt; HTML
                &lt;tr&gt;
                  &lt;td&gt;{$procArray[0]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[1]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[2]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[3]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[4]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[5]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[6]}&lt;/td&gt;
                  &lt;td&gt;{$escedprocArray7}&lt;/td&gt;
                  &lt;td&gt;{$procArray[8]}&lt;/td&gt;
                  &lt;td&gt;{$procArray[9]}&lt;/td&gt;
                  &lt;td&gt;{$cmd}&lt;/td&gt;
                &lt;/tr&gt;
HTML;
                    }
                }

                array_pop($userInfo);

                preg_match_all('/(\\d)+ user/', $shellResult["uptime"], $reslut);
                ?&gt;
                当前&lt;?= $reslut[1][0];?&gt;名用户在线，&lt;?= count($userInfo);?&gt;名用户存在进程.
                &lt;table class="table table-striped table-bordered table-condensed"&gt;
                    &lt;thead&gt;
                    &lt;tr&gt;
                        &lt;th&gt;用户名&lt;/th&gt;
                        &lt;th&gt;进程数&lt;/th&gt;
                        &lt;th&gt;CPU占用&lt;/th&gt;
                        &lt;th&gt;物理内存占用&lt;/th&gt;
                        &lt;th&gt;虚拟内存占用&lt;/th&gt;
                    &lt;/tr&gt;
                    &lt;/thead&gt;
                    &lt;tbody&gt;
                    &lt;? foreach($userInfo as $k =&gt; $v): ?&gt;
                        &lt;tr&gt;
                            &lt;td&gt;&lt;?= $k; ?&gt;&lt;/td&gt;
                            &lt;td&gt;&lt;?= $v["procNum"]; ?&gt;&lt;/td&gt;
                            &lt;td&gt;&lt;?= $v["cpuPer"]; ?&gt;%&lt;/td&gt;
                            &lt;td&gt;&lt;?= round($v["realMem"]/1024); ?&gt; MB (&lt;?= $v["memPer"]; ?&gt;%)&lt;/td&gt;
                            &lt;td&gt;&lt;?= round($v["swapMem"]/1024); ?&gt; MB&lt;/td&gt;
                        &lt;/tr&gt;
                    &lt;? endforeach; ?&gt;
                    &lt;/tbody&gt;
                &lt;/table&gt;
            &lt;/section&gt;
            &lt;section id="process"&gt;
                &lt;header&gt;进程&lt;/header&gt;
                &lt;table class="nopadding table table-striped table-bordered table-condensed procTable code"&gt;
                    &lt;thead&gt;
                    &lt;tr&gt;
                        &lt;th&gt;USER&lt;/th&gt;
                        &lt;th&gt;PID&lt;/th&gt;
                        &lt;th&gt;%CPU&lt;/th&gt;
                        &lt;th&gt;%MEM&lt;/th&gt;
                        &lt;th&gt;VSZ&lt;/th&gt;
                        &lt;th&gt;RES&lt;/th&gt;
                        &lt;th&gt;TTY&lt;/th&gt;
                        &lt;th&gt;STAT&lt;/th&gt;
                        &lt;th&gt;START&lt;/th&gt;
                        &lt;th&gt;TIME&lt;/th&gt;
                        &lt;th&gt;COMMAND&lt;/th&gt;
                    &lt;/tr&gt;
                    &lt;/thead&gt;
                    &lt;tbody&gt;
                    &lt;?= $procOutput; ?&gt;
                    &lt;/tbody&gt;
                &lt;/table&gt;
                &lt;hr /&gt;
                &lt;div class="accordion-group"&gt;
                    &lt;div class="accordion-heading"&gt;
                        &lt;a class="btn btn-info" data-toggle="collapse" href="#prePs"&gt;
                            查看原始命令
                        &lt;/a&gt;
                    &lt;/div&gt;
                    &lt;div id="prePs" class="accordion-body collapse"&gt;
                        &lt;pre&gt;&lt;?= htmlspecialchars($shellResult["ps"]);?&gt;&lt;/pre&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/section&gt;
            &lt;/section&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class="pull-right"&gt;
        &lt;script type="text/javascript" src="//static2.jybox.net/my-website/analyzer.js"&gt;&lt;/script&gt;
        精英王子
    &lt;/div&gt;
&lt;/div&gt;
&lt;script type='text/javascript' src='http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js'&gt;&lt;/script&gt;
&lt;script type='text/javascript' src='http://lib.sinaapp.com/js/bootstrap/latest/js/bootstrap.min.js'&gt;&lt;/script&gt;
&lt;!--[if lte IE 8]&gt;
&lt;script type='text/javascript' src='//static2.jybox.net/tools/kill-ie6.js'&gt;&lt;/script&gt;
&lt;![endif]--&gt;
&lt;/body&gt;
&lt;/html&gt;
</pre></body></html>